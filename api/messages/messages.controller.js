const MessagesModel = require('./messages.model');

// const mongoose = require('mongoose');

exports.addNewMessage = (req, res) => {

    // mongoose.model("MessagesModel").create(req.body, (err, doc) => {
    //     res.send(doc);
    // });
    //

    MessagesModel.create(req.body, (err, doc) => {
        res.send(doc);
    });
};

exports.showMessages = (req, res) => {
    let query = req.query;
    console.log(query);
    MessagesModel
        .findActiveMessages(query) //  вызов нового метода из модели, там определили этот метод
        .exec((err, docs) => {
            if (err) return res.status(400).send({error: err.message});
            let order = "";
            for (let doc of docs) {
                let date = doc.createdAt;
                doc.time = date.getHours().toString().padStart(2, '0') + ":" + date.getMinutes().toString().padStart(2, '0') + ":" + date.getSeconds().toString().padStart(2, '0');
            }
            let sortField = Object.keys(query)[0];
            if (req.query && req.query[sortField] == "desc") {
                order = "asc";
            } else {
                order = "desc"
            }
            res.render('messages.twig', {docs, order});
        });
};

exports.showMessage = (req, res) => {
    let query = req.path;
    console.log(query);
    MessagesModel
        .findActiveMessage(query) //  вызов одной записи
        .exec((err, doc) => {
            if (err) return res.status(400).send({error: err.message});
            let date = doc.createdAt;
            doc.time = date.getHours().toString().padStart(2, '0') + ":" + date.getMinutes().toString().padStart(2, '0') + ":" + date.getSeconds().toString().padStart(2, '0');
            res.render('message.twig', {doc});
        });
};