const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessagesSchema = new Schema({
    username: {type: String},
    message: {type: String},
    show: {type: Date, expires: 0, index: -1}, // что значит -1
    // addedAt: {type: Date, default: Date.now}
}, {
    timestamps: true,
    collection: "MessagesCollection"
});

MessagesSchema.statics = {
    findActiveMessages: function (query) {
        let sortField = Object.keys(query)[0];
        let sort = query[sortField];
/*        console.log(query);
        console.log(sortField);*/
        if (sortField && sort) {
            sortField = (sortField == "time") ? "createdAt" : sortField;
            let queryParam = {};
            queryParam[sortField] = sort;
            return this.find().sort(queryParam);
        } else {
            return this.find({show: {$gte: new Date()}});
        }
    },
    findActiveMessage: function (query) {
        let id = query.slice(1);
        console.log(id);
        return this.findById(id);
    }
};

MessagesSchema.pre('save', function () {
    if(this.isNew) { // что это?
        this.show = new Date(new Date().getTime() + this.show * 1000);
    }
});

module.exports = mongoose.model("MessagesModel", MessagesSchema);